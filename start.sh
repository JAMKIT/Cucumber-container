#!/bin/bash
set -e

CPATH=${CPATH:=}
FEATURE=${FEATURE:=}

Xvfb -ac :99 -screen 0 1920x1200x16 &
export DISPLAY=:99

cd $CPATH
selenium-cucumber gen
cucumber features $FEATURE
