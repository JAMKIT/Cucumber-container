CPATH = testin kansiopolku
FEATURE = testitiedosto

$PWD on Linuxin ympäristömuuttuja, joka viittaa. Voit vaihtaa tilalle absoluuttisen tai relatiivisen polun.

docker run -it --rm --name sc -e CPATH=$PWD -e FEATURE=testi.feature -v $PWD:$PWD registry.gitlab.com/jamkit/cucumber-container/cucumber