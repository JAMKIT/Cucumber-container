# Selenium-Cucumber installation Dockerfile Mikko Siloaho 13.2.2017
FROM ubuntu:latest

# Install ruby, gem and cucumber
RUN apt-get update -y -qq  && apt-get install -y -qq wget &&\
    apt-get install -y -qq ruby-full &&\
    apt-get install -y -qq rubygems &&\
    apt-get install -y -qq rails &&\
    apt-get install -y -qq firefox &&\
    apt-get install -y -qq xvfb&&\
    gem install selenium-cucumber &&\
    gem install xpath &&\
    apt-get install -y -qq xserver-xephyr

RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.14.0/geckodriver-v0.14.0-linux64.tar.gz &&\
    tar xfvz geckodriver-v0.14.0-linux64.tar.gz && rm -f geckodriver-v0.14.0-linux64.tar.gz &&\
    chmod +777 geckodriver && mv -f geckodriver /usr/bin/geckodriver

COPY start.sh /usr/local/bin/start.sh

RUN chmod -R 777 /usr/local/bin/start.sh

ENTRYPOINT ["/usr/local/bin/start.sh"]

